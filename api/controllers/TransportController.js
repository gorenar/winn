/**
 * TransportController
 *
 * @description :: Server-side logic for managing transports
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */

module.exports = {
	getTransport: function(req, res) {
		Transport.find({}, function(err, found){
	    			res.view( 'homepage', {transports: found} );
			});
	}
};
