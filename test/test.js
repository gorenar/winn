var Transport = require('../api/models/Transport');
var chai = require('chai');
var chaiHttp = require('chai-http');
var sails = require('sails');
var should = chai.should();
var server = require('express');

chai.use(chaiHttp);
describe('transport', function() {

  before(function(done) {

    // Increase the Mocha timeout so that Sails has enough time to lift.
    this.timeout(50000);

    sails.lift({
      // configuration for testing purposes
    }, function(err, server) {
      if (err) return done(err);
      // here you can load fixtures, etc.
      done(err, sails);
    });
  });

  after(function(done) {
    // here you can clear fixtures, etc.
    sails.lower(done);
  });

  describe('/GET transport', function() {
    it('should list ALL transport on /transport GET', function(done) {
      chai.request("http://localhost:3000")
      .get('/transport')
      .end(function(err, res) {
        res.should.have.status(200);
        done();
      });
    });

    it('should list a SINGLE transport on /transport/<id> GET', function(done) {
      chai.request("http://localhost:3000")
        .get('/transport')
        .end(function(err, res){
          res.should.have.status(200);
          res.should.be.json;
          res.body.should.be.a('array');
          done();
        });
    });
  });

  describe('/POST transport', function() {
    it('should add a SINGLE transport on /transport POST', function(done){
            chai.request("http://localhost:3000")
            .post('/transport')
            .send({title: "test",
                    departure_date: "1995-05-03",
                    arrival_date: "1995-05-03",
                    departure_point: "49.2039,-0.297232",
                    arrival_point: "49.1897329,-0.264633",
                    status: "PROPOSED"})
            .end(function(err, res){
                res.should.have.status(201);
                res.should.be.json;
                res.body.should.be.a('object');
                res.body.should.have.property('title');
                res.body.should.have.property('departure_date');
                res.body.should.have.property('arrival_date');
                res.body.should.have.property('departure_point');
                res.body.should.have.property('arrival_point');
                res.body.should.have.property('status');
              done();
            });
      });

  });
  describe('/PUT transport', function() {
    it('should update a SINGLE transport on /transport/<id> PUT', function(done){
            chai.request("http://localhost:3000")
            .get('/transport')
            .end(function(err, res) {
              chai.request("http://localhost:3000")
              .put('/transport/'+res.body[0].id)
              .send({title: "test",
                      departure_date: "1995-05-03",
                      arrival_date: "1995-05-03",
                      departure_point: "49.2039,-0.297232",
                      arrival_point: "49.1897329,-0.264633",
                      status: "PROPOSED"})
              .end(function(err, res){
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('title');
                  res.body.should.have.property('departure_date');
                  res.body.should.have.property('arrival_date');
                  res.body.should.have.property('departure_point');
                  res.body.should.have.property('arrival_point');
                  res.body.should.have.property('status');
                done();
              });
            });
      });
  });

  describe('/DELETE transport', function() {
    it('should delete a SINGLE transport on /transport/<id> DELETE', function(done){
            chai.request("http://localhost:3000")
            .get('/transport')
            .end(function(err, res) {
              chai.request("http://localhost:3000")
              .delete('/transport/'+res.body[0].id)
              .end(function(err, res){
                  res.should.have.status(200);
                  res.should.be.json;
                  res.body.should.be.a('object');
                  res.body.should.have.property('title');
                  res.body.should.have.property('departure_date');
                  res.body.should.have.property('arrival_date');
                  res.body.should.have.property('departure_point');
                  res.body.should.have.property('arrival_point');
                  res.body.should.have.property('status');
                done();
              });
            });
      });
  });
});
