define({ "api": [
  {
    "description": "<p>Show form update</p>",
    "type": "",
    "url": "",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": ""
  },
  {
    "type": "DELETE",
    "url": "transport/:id",
    "title": "",
    "description": "<p>Delete the selected transport</p>",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": "DeleteTransportId"
  },
  {
    "type": "GET",
    "url": "transport",
    "title": "",
    "description": "<p>GET all transport</p>",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": "GetTransport"
  },
  {
    "type": "POST",
    "url": "transport/:id",
    "title": "",
    "description": "<p>Created a new transport</p>",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": "PostTransportId"
  },
  {
    "type": "PUT",
    "url": "transport/:id",
    "title": "",
    "description": "<p>Here we are creating an 'update' method</p>",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": "PutTransportId"
  },
  {
    "type": "PUT",
    "url": "transport/:id",
    "title": "",
    "description": "<p>Update the selected transport</p>",
    "version": "0.0.0",
    "filename": "assets/js/app.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_app_js",
    "name": "PutTransportId"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "private",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Private"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  },
  {
    "type": "",
    "url": "public",
    "title": "",
    "version": "0.0.0",
    "filename": "assets/js/dependencies/sails.io.js",
    "group": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "groupTitle": "C__Users_utilisateur_Desktop_winn_assets_js_dependencies_sails_io_js",
    "name": "Public"
  }
] });
