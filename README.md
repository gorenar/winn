# winn

a [Sails](http://sailsjs.org) application

# INSTALLATION
need to install dependencies :
  - `npm install`

# CONFIG
configure connection settings :
  - `./config/connection.js`
  - `change parameters of localMysqlServer`

# INFO
routes :
  - `./config/routes.js`
cors :
  - `./config/cors.js`
js :
  - `./assets/js`
css :
  - `./assets/styles`
models :
  - `./api/models`

# START PROGRAM
  - `create database winn`
  - `start mysql`
  - `sails lift` OR `npm start`
