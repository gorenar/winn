var app = angular.module('app', ['ngRoute','ngResource']);
/**
 * @api {PUT} transport/:id
 * @apiDescription Here we are creating an 'update' method
 */
app.factory('Transport', ['$resource', function($resource){
    return $resource('http://localhost:3000/transport/:id', null, {
      'update': { method:'PUT', params:{id: "@id"} }
    });
  }]);


// controller
app.controller('TransportsCtrl', function($scope, Transport){
  /**
   * @api {GET} transport
   * @apiDescription GET all transport
   */
   $scope.transports = Transport.query();
   $scope.transport = false;
   $scope.n = {};

   /**
    * @api {PUT} transport/:id
    * @apiDescription Update the selected transport
    */
   $scope.editTransport = function(){
      $scope.transport.$update(function(){
        $scope.transport = false;
        alert("this transport updated");
      });
    };

    /**
     * @apiDescription Show form update
     */
     $scope.showEditTransport = function(transport){
        $scope.transport =transport;
     };

     $scope.showMapTransport = function(transport){
        console.log("test");
     };

    /**
     * @api {POST} transport/:id
     * @apiDescription Created a new transport
     */
    $scope.newTransport = function(transport){
        var transport = Transport.save(null, $scope.n, function(){
          $scope.transports.push(transport);
          alert("A new transport created");
        })
    };

    /**
     * @api {DELETE} transport/:id
     * @apiDescription Delete the selected transport
     */
    $scope.remove = function(index){
      var r = confirm("You're want to delete this transport?");
      if (r == true) {
        Transport.remove({id: index}, function(){
          $scope.transports = Transport.query();
        });
      }

    };
});
